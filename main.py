import genanki
from googletrans import Translator
from os import system
import sys
import urllib, socket
import requests
import random
from time import sleep
google_tr = Translator()
from selenium import webdriver
import urllib
from selenium.webdriver.common.keys import Keys

def get_random_id(length):
    random_id = ''
    for i in range(11):
        random_id += str(random.randint(1,9))
    return int(random_id)

driver = webdriver.Firefox()
def more_info(word):
    url = 'https://translate.google.com/#view=home&op=translate&sl=auto&tl=fa'
    driver.get(url)
    text_area = driver.find_element_by_xpath('//*[@id="source"]')
    text_area.click()
    text_area.send_keys(word)
    sleep(1)
    name = f'{word}_extra.png'
    driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/div[2]/div[2]/div[3]/div[3]/div[1]').screenshot('images/'+name)
    return name

def get_image(word):
    
    #word="apple"
    url="http://images.google.com/search?q="+word+"&tbm=isch&sout=1"
    driver.get(url)
    imageXpathSelector='/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div/div[1]/div[1]/div[1]/a[1]/div[1]/img'
    img=driver.find_element_by_xpath(imageXpathSelector)
    #src=(img.get_attribute('src'))
    #r = requests.get(src, allow_redirects=True)
    name = word+'.png'
    img.screenshot(f'images/{name}')
    #with open(f'images/{name}', 'wb') as f:
    #    f.write(img.screenshot(f'images/{name}'))
    #open(f'images/{name}', 'wb').write(r.content)
    #urllib.urlretrieve(src, 'images/'+word+".jpg")
    return name
    
def get_image1(word):
    query = word
    r = requests.get("https://api.qwant.com/api/search/images", timeout=5,
        params={'count': 50, 'q': query, 't': 'images', 'safesearch': 1, 'locale': 'en_US', 'uiv': 4},
        headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
        }
    )
    response = r.json().get('data').get('result').get('items')
    print(response)
    urls = [r.get('media') for r in response]
    #print(urls)
    url = random.choice(urls)
    print(url)

    name = word.strip() + '.' + url.split('.')[-1]
    while True:
        try:
            r = requests.get(url, allow_redirects=True)
            open(f'images/{name}', 'wb').write(r.content)
            break
        except urllib.error.URLError or socket.timeout:
            print('ops, getting new url')
            url = random.choice(urls)
            print(url)
    #command = f'wget -O images/{name} """{url}"""'
    #system(command)
    #print(random.choice(urls))
    return name

def get_voice(word):
    command = f'''wget -q -U Mozilla -O voices/{word}.mp3 translate.google.com.vn/translate_tts?ie=UTF-8\&q={word}\&tl=en\&client=tw-ob'''
    system(command)
    return str(word + '.mp3')

if len(sys.argv) > 1:
    print('file located at this address:', sys.argv[1])
    with open(sys.argv[1], 'r') as f:
        word_list = f.readlines()
        
#for i in word_list:
#    if not i.strip():
#        continue
#    print(i)
#    get_voice(i.strip())
#exit()

# Use genanki to define a flashcard model
my_model = genanki.Model(
    get_random_id(10),
    'Simple Model',
    fields=[
        {'name': 'Question'},
        {'name': 'Answer'},
        {'name': 'Image'},
        {'name': 'Voice'},
        {'name':'Extra'},
    ],
    templates=[
        {
            'name': 'Card 1',
            'qfmt': """{{Image}}<br>{{Voice}}<br>{{Question}} """,
            'afmt': '{{FrontSide}}<hr id="answer"> {{Answer}}<br>{{Extra}}',
        },
    ], css=".card {\n font-family: arial;\n font-size: 20px;\n text-align: center;\n color: black;\n background-color: white;\n}\n")
deck_name = 'TestV3v1_'+ str(get_random_id(10)) 
# Specify the deck with genanki
my_deck = genanki.Deck(get_random_id(10),deck_name)

voices = []
images = []
extras = []
for this in word_list:
    sleep(1)
    if not this.strip():
        continue
    print(this)
    word = this.strip()
    translated_word = google_tr.translate(word, dest='fa').text
    voice = get_voice(word)
    voices.append('voices/'+voice)
    sleep(0.3)
    image = get_image(word)
    sleep(0.5)
    images.append('images/'+image)
    try:
        extra = more_info(word)
        sleep(0.4)
        extras.append('images/'+extra)
    except:
        extra = 'image_not_found.jpg'
    aNote = genanki.Note(
            model=my_model, fields=[ str(word), str(translated_word), f'<img src="{image}">', f'[sound:{voice}]', f'<img src="{extra}">']
    )
    my_deck.add_note(aNote)

driver.close()
# Output anki file in desired folder
my_package = genanki.Package(my_deck)
my_package.media_files =  images + voices + extras
package_name = deck_name +'.apkg'
my_package.write_to_file(package_name)
print('package created with name:', package_name)


